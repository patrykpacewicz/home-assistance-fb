'use strict';

const ms = require('ms');
const express = require('express');
const functions = require('firebase-functions');
const admin = require('firebase-admin'); admin.initializeApp();
const firestore = admin.firestore()

const token = "cg816eyt8nRnkW4y"

const adminApp = express();
const accessApp = express();

// GET /admin/access/?token=ABC
adminApp.get('/admin/access', async (req, res) => {
    if (req.query.token !== token) return res.status(403).json({error: "Incorrect token"})
    const time = admin.firestore.Timestamp.now()
    const accessData = await firestore.collection('accesses').get()
    const data = accessData.docs.map(a => { const d = a.data() ; return {
        id: a.id,
        createdAt: d.createdAt.toDate().toString(),
        isActive: (d.createdAt.seconds + d.ttlSec) > time.seconds
    }})

    return res.json(data)
});

// POST /access/?token=ABC&time=1h
accessApp.post('/access/', async (req, res) => {
    if (req.query.token !== token) return res.status(403).json({error: "Incorrect token"})
    if (!req.query.time) return res.status(400).json({error: "Incorrect time"})

    const ttlSec = ms(req.query.time) / 1000 
    if (!ttlSec) return res.status(400).json({error: "Incorrect time"})

    const accessData = await firestore.collection('accesses').add({
        createdAt: admin.firestore.Timestamp.now(),
        ttlSec: ttlSec
    });
    
    return res.json({id: accessData.id})
});

// PUT /access/{ID}/action/open-garage
accessApp.put('/access/:id/action/:action', async (req, res) => {
    if (!req.params.id) return res.status(403).json({error: "Incorrect access-id"})
    if (!req.params.action) return res.status(400).json({error: "Incorrect action name"})

    const id = req.params.id
    const action = req.params.action
    const time = admin.firestore.Timestamp.now()
    const accessData = await firestore.collection('accesses').doc(req.params.id).get()
    const data = accessData.data()

    if (!data) return res.status(403).json({error: "The access-id does not exists"})
    if ((data.createdAt.seconds + data.ttlSec) < time.seconds) return res.status(403).json({error: "The access-id expired"})

    // DO ACTION HERE

    return res.status(200).json({id, time, data, action})
});

exports.access = functions.https.onRequest(accessApp);
exports.admin  = functions.https.onRequest(adminApp);